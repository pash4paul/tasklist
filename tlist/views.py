#-*- coding: utf-8 -*-
import json
from django.core.serializers import serialize
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from tastypie.http import HttpBadRequest
from tlist.models import TList


def index(request):
    return render(request, 'tlist.html', {'title': u'Список задач'})


def api_get_tasks_tree(request):
    data = []
    use_item = []
    make_tree(TList.objects.all(), data, use_item)
    return HttpResponse(json.dumps(data))


def make_tree(objects, out, use_item):
    for item in objects:
        if item not in use_item:
            obj = {'id': str(item.pk), 'label': item.task, 'children': []}
            if item.children.all() != []:
                make_tree(item.children.all(), obj['children'], use_item)
            use_item.append(item)
            out.append(obj)


@csrf_exempt
def api_add_task(request):
    if request.method == 'POST':
        tList = TList()
        for key, value in json.loads(request.body).items():
            if (key == 'parent'):
                tList.parent = TList.objects.get(pk=int(value))
                continue
            setattr(tList, key, value)
        tList.save()
        data = serialize('json', [tList,])
        return HttpResponse(data, content_type="application/json")
    return HttpBadRequest()


@csrf_exempt
def api_update_task(request):
    if request.method == 'POST':
        pk = json.loads(request.body)['pk']
        task = json.loads(request.body)['task']
        tList = TList.objects.get(pk=pk)
        tList.task = task
        tList.save()
        return HttpResponse()
    return HttpBadRequest()


@csrf_exempt
def api_remove_task(request):
    if request.method == 'POST':
        pk = json.loads(request.body)['pk']
        TList.objects.get(pk=pk).delete()
        return HttpResponse()
    return HttpBadRequest()