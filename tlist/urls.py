from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView


urlpatterns = patterns('',
    url(r'^$', 'tlist.views.index', name='tlist-index'),
    url(r'^api/get/tasks/tree/$', 'tlist.views.api_get_tasks_tree', name='tlist-api-get-tasks-tree'),

    url(r'^api/add/task/$', 'tlist.views.api_add_task', name='tlist-api-add-task'),
    url(r'^api/update/task/$', 'tlist.views.api_update_task', name='tlist-api-update-task'),
    url(r'^api/remove/task/$', 'tlist.views.api_remove_task', name='tlist-api-remove-task'),
)
