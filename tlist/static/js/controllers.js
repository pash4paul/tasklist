var appTaskList = angular.module('appTaskList',['angularTreeview']);

appTaskList.controller('TaskCtrl', ['$scope', '$http', function($scope, $http) {
  $scope.reload_data = function() {
    $http({method: 'GET', url: '/api/get/tasks/tree'}).success(function(data, status, headers, config) {
      $scope.tasks = data;
    });
  };

  $scope.reload_data();

  $scope.$watch( 'tasksid.currentNode', function( newObj, oldObj ) {
    if( $scope.tasksid && angular.isObject($scope.tasksid.currentNode) ) {
      $scope.task = {pk: $scope.tasksid.currentNode.id, task: $scope.tasksid.currentNode.label};
      $scope.updateResult = "";
      $('#myModal').modal('show');
    }
  }, false);

  $scope.init_subtask = function() {
    $scope.updateResult = "";
    $('#modalSubtask').modal('show');
  };

  $scope.create = function() {

    var task_text = "";
    
    if ($scope.task.text_subtask !== "" && $scope.task.text_subtask !== undefined) {
      task_text = $scope.task.text_subtask;
    } else {
      task_text = $scope.task.text;
      $scope.task.pk = undefined;
    }

    $http({method: 'POST', url: '/api/add/task/',
      data: {task: task_text, parent: $scope.task.pk},
      headers: {'Content-Type': 'application/json; charset=utf-8'}})
      .success(function (data, status, headers, config) {
        if (status === 200) {
          $scope.reload_data();
          $scope.updateResult = "Подзадача добавлена";
        }
        $scope.task.text_subtask = "";
        $scope.task.text = "";
      });
  };

  $scope.update = function() {
    $http({method: 'POST', url: '/api/update/task/',
      data: {pk: $scope.task.pk, task: $scope.task.task},
      headers: {'Content-Type': 'application/json; charset=utf-8'}})
      .success(function (data, status, headers, config) {
        if (status === 200) {
          $scope.reload_data();
          $scope.updateResult = "Изменения сохранены";
        }
        else {
          $scope.updateResult = "Ошибка при изменение данных";
        }
      });
  };

  $scope.remove = function() {
    $http({method: 'POST',
      url: '/api/remove/task/',
      data: {pk: $scope.task.pk},
      headers: {'Content-Type': 'application/json; charset=utf-8'}})
      .success(function (data, status, headers, config) {
        if (status === 200) {
          $scope.reload_data();
          $('#myModal').modal('hide');
        }
      });
  };
}]);