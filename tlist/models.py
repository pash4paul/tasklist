from django.core import serializers
from django.db import models


class TList(models.Model):
    task = models.CharField(max_length=512)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children')